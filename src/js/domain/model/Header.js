const {
    ConversionService
} = require('../../service/ConversionService');
const {
    validateRequiredFields,
    validateIPAddress,
    validateNumericType,
    validateProtocol
} = require('./Validator');

/**
 * This class is the header of the DatagramIP
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class Header {

    /**
     * This constructor allows to create Headers IP object
     * 
     * @param {Number} totalLength a number for the total length of the header
     * @param {Array} flags an array of numbers with only 3 positions [0, 1||0, 1||0] for the flags
     * @param {Number} fragmentOffset a number for the fragment off set
     * @param {Number} protocol could be TCP, ICMP or UDP ids 
     * @param {String} sourceIP an IP address in IPv4
     * @param {String} destinationIP an IP address in IPv4
     * 
     */
    constructor(totalLength, flags, fragmentOffset, protocol, sourceIP, destinationIP) {

        //Validate all the required fields

        validateRequiredFields(totalLength, "Es necesario ingresar la longitud total");
        validateRequiredFields(fragmentOffset, "Es necesario ingresar el desplazamiento");
        validateRequiredFields(protocol, "Es necesario ingresar el protocolo");
        validateRequiredFields(sourceIP, "Es necesario ingresar la dirección IP de origen");
        validateRequiredFields(destinationIP, "Es necesario ingresar la dirección IP de destino");
        validateProtocol(protocol, "Es necesario ingresar el protocolo");
        //Validate the numeric fields

        validateNumericType(totalLength, "La longitud total debe ser un número");
        validateNumericType(fragmentOffset, "El desplazamiento debe ser un número");
        //Validate the IP addresses

        validateIPAddress(sourceIP, "La dirección IP de origen no es correcta");
        validateIPAddress(destinationIP, "La dirección IP de destino no es correcta");


        this.version = 4;
        this.headerLength = 5;
        this.services = 0;
        this.totalLength = totalLength;
        this.flags = flags;
        this.fragmentOffset = fragmentOffset;
        this.protocol = protocol;
        this.sourceIP = sourceIP;
        this.destinationIP = destinationIP;
        this.id = this.generateID();
        this.ttl = this.generateTTL();
        this.checksum = this.getChecksum();
    }


    /**
     * This method generates the id for the header
     * @returns {Number} could be a number between 0-65535
     */
    generateID() {
        return Math.floor(Math.random() * (65535));
    }

    /**
     * This method generates the ttl for the header
     * @returns {Number} could be a number between 0-255
     */
    generateTTL() {
        return Math.floor(Math.random() * (255));
    }

    /**
     * 
     * This method allows to get the format presentation for the frontend
     * gets all the components of the header
     * 
     * @returns {String} with the hex format
     */
    getHexFormatPresentation() {
        let versionHex = ConversionService.getStringHexNumber(this.version);
        let headerLengthHex = ConversionService.getStringHexNumber(this.headerLength);
        let servicesHex = ConversionService.getStringHexNumber(this.services);
        let totalLengthHex = ConversionService.getStringHexNumber(this.totalLength);
        let idHex = ConversionService.getStringHexNumber(this.id);

        let flagsHex = this.flags.toString().replaceAll(",", ""); //3bits
        let offsetHex = ConversionService.getFormat13bits(ConversionService.getStringBinaryNumber(this.fragmentOffset)); //13 bits
        let flags_offset = parseInt(ConversionService.getStringDecimalNumber(flagsHex + offsetHex));
        let flags_offsetHex = ConversionService.getStringHexNumber(flags_offset);

        let ttlHex = ConversionService.getStringHexNumber(this.ttl);
        let protocolHex = ConversionService.getStringHexNumber(this.protocol);

        let sourceIPArray = this.sourceIP.split('.');


        let sourceIP1 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[0]));
        let sourceIP2 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[1]));
        let sourceIP3 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[2]));
        let sourceIP4 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[3]));

        let destinationIPArray = this.destinationIP.split('.');
        let destinationIP1 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[0]));
        let destinationIP2 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[1]));
        let destinationIP3 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[2]));
        let destinationIP4 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[3]));
        let totalLengthHexAux = ConversionService.getFormat16Bits(totalLengthHex);
        let flags_offsetAux = ConversionService.getFormat16Bits(flags_offsetHex);
        let out = versionHex + headerLengthHex + ' ' + ConversionService.getFormatHex(servicesHex) + ' ' +
            totalLengthHexAux.substring(0, (totalLengthHexAux.length / 2)) + ' ' + totalLengthHexAux.substring((totalLengthHexAux.length / 2), totalLengthHexAux.length) + ' ' +
            ConversionService.getFormatHex(idHex.substring(0, (idHex.length / 2))) + ' ' + ConversionService.getFormatHex(idHex.substring((idHex.length / 2), idHex.length)) + ' ' +
            flags_offsetAux.substring(0, (flags_offsetAux.length / 2)) + ' ' + flags_offsetAux.substring((flags_offsetAux.length / 2), flags_offsetAux.length) + ' ' +
            ConversionService.getFormatHex(ttlHex) + ' ' + ConversionService.getFormatHex(protocolHex) + ' ' +
            ConversionService.getFormatHex(this.checksum.substring(0, (this.checksum.length / 2))) + ' ' + ConversionService.getFormatHex(this.checksum.substring((this.checksum.length / 2), this.checksum.length)) + ' ' +
            ConversionService.getFormatHex(sourceIP1) + ' ' + ConversionService.getFormatHex(sourceIP2) + ' ' +
            ConversionService.getFormatHex(sourceIP3) + ' ' + ConversionService.getFormatHex(sourceIP4) + ' ' +
            ConversionService.getFormatHex(destinationIP1) + ' ' + ConversionService.getFormatHex(destinationIP2) + ' ' +
            ConversionService.getFormatHex(destinationIP3) + ' ' + ConversionService.getFormatHex(destinationIP4) + ' ';

        return out;
    }


    /**
     * This method allows to get the format in hex without the checksum
     * 
     * @returns {String} with the hex format
     */
    getHexFormatWithoutChecksum() {
        let versionHex = ConversionService.getStringHexNumber(this.version);
        let headerLengthHex = ConversionService.getStringHexNumber(this.headerLength);
        let servicesHex = ConversionService.getStringHexNumber(this.services);
        let totalLengthHex = ConversionService.getStringHexNumber(this.totalLength);
        let idHex = ConversionService.getStringHexNumber(this.id);

        let flagsHex = this.flags.toString().replaceAll(",", ""); //3bits
        let offsetHex = ConversionService.getFormat13bits(ConversionService.getStringBinaryNumber(this.fragmentOffset)); //13 bits
        let flags_offset = parseInt(ConversionService.getStringDecimalNumber(flagsHex + offsetHex));
        let flags_offsetHex = ConversionService.getStringHexNumber(flags_offset);

        let ttlHex = ConversionService.getStringHexNumber(this.ttl);
        let protocolHex = ConversionService.getStringHexNumber(this.protocol);

        let sourceIPArray = this.sourceIP.split('.');

        let sourceIP1 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[0]));
        let sourceIP2 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[1]));
        let sourceIP3 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[2]));
        let sourceIP4 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[3]));

        let destinationIPArray = this.destinationIP.split('.');
        let destinationIP1 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[0]));
        let destinationIP2 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[1]));
        let destinationIP3 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[2]));
        let destinationIP4 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[3]));
        let totalLengthHexAux = ConversionService.getFormat16Bits(totalLengthHex);
        let flags_offsetAux = ConversionService.getFormat16Bits(flags_offsetHex);
        let out = versionHex + headerLengthHex + ' ' + ConversionService.getFormatHex(servicesHex) + ' ' +
            totalLengthHexAux.substring(0, (totalLengthHexAux.length / 2)) + ' ' + totalLengthHexAux.substring((totalLengthHexAux.length / 2), totalLengthHexAux.length) + ' ' +
            ConversionService.getFormatHex(idHex.substring(0, (idHex.length / 2))) + ' ' + ConversionService.getFormatHex(idHex.substring((idHex.length / 2), idHex.length)) + ' ' +
            flags_offsetAux.substring(0, (flags_offsetAux.length / 2)) + ' ' + flags_offsetAux.substring((flags_offsetAux.length / 2), flags_offsetAux.length) + ' ' +
            ConversionService.getFormatHex(ttlHex) + ' ' + ConversionService.getFormatHex(protocolHex) + ' ' +
            ConversionService.getFormatHex(sourceIP1) + ' ' + ConversionService.getFormatHex(sourceIP2) + ' ' +
            ConversionService.getFormatHex(sourceIP3) + ' ' + ConversionService.getFormatHex(sourceIP4) + ' ' +
            ConversionService.getFormatHex(destinationIP1) + ' ' + ConversionService.getFormatHex(destinationIP2) + ' ' +
            ConversionService.getFormatHex(destinationIP3) + ' ' + ConversionService.getFormatHex(destinationIP4) + ' ';

        return out;
    }

    /**
     * This method allows to get the checksum
     * 
     * @returns {String} with the format
     */
    getChecksum() {
        let formatHex = this.getHexFormatWithoutChecksum().split(' ');
        let pairs = [];

        for (let i = 0; i < formatHex.length - 1; i += 2) {
            pairs.push(formatHex[i] + formatHex[i + 1]);
        }
        let checksum = 0;
        let numberA = pairs[0];
        let numberB = pairs[1];
        for (let i = 2; i < pairs.length; i++) {
            checksum = ConversionService.getStringHexNumber(ConversionService.sumHexNumbers(numberA, numberB));
            numberA = checksum;
            numberB = pairs[i];
        }
        checksum = ConversionService.getStringHexNumber(ConversionService.sumHexNumbers(numberA, numberB));
        return ConversionService.setComplementA1(checksum);
    }
    /**
     * This method allows to get the Wireshark presentation 
     * @returns {String} with the format required
     */
    getWireSharkRepresentation() {
        let out = "Internet Protocol Version: " + this.version +
            "\nLongitud de Encabezado: " + this.headerLength +
            "\nServicios Diferenciados:" + this.services +
            "\nLongitud Total:" + this.totalLength +
            "\nIdentificacion:" + this.id +
            "\nFlags: " + this.flags[0] +
            "\n DF:" + this.flags[1] +
            "\n MF:" + this.flags[2] +
            "\nDesplazamiento: " + this.fragmentOffset +
            "\nTiempo de Vida: " + this.ttl +
            "\nProtocolo: " + this.protocol +
            "\nChecksum: " + this.checksum +
            "\nDireccion  de  Origen: " + this.sourceIP +
            "\nDireccion  de  Destino: " + this.destinationIP;
        return out;
    }
    /**
     * This method allows to get the binary representation 
     * @returns {String} with the format 
     */
    getBinaryRepresentation() {
        let hex = this.getHexFormatPresentation();
        let hexArray = hex.split(' ');
        let binary = '';
        let counter = 0;
        for (let i = 0; i < hexArray.length - 1; i++) {
            let numberA = hexArray[i][0];
            let numberB = hexArray[i][1];
            let numberAConverted = ConversionService.getBinaryStringByHex(numberA);
            let numberBConverted = ConversionService.getBinaryStringByHex(numberB);
            let numberABinary = ConversionService.getFormat16Bits(numberAConverted);
            let numberBBinary = ConversionService.getFormat16Bits(numberBConverted);
            binary += ConversionService.addSpacesBinary(numberABinary) + ConversionService.addSpacesBinary(numberBBinary);
            counter += 8;
            if (counter == 32) {
                binary += "\n";
                counter = 0;
            }
        }
        return binary;
    }
}

module.exports = {
    Header
};