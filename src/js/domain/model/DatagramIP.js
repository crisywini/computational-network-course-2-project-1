
const {Header} = require('./Header');

/**
 * This class is the datagramIP
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class DatagramIP{
    /**
     * 
     * This constructor allows to create DatagramIP objects
     * 
     * @param {Header} header of the datagram needs the correct data
     * 
     */
    constructor(header){
        this.header = header;
    }
}

module.exports = {
    DatagramIP
};