const {RequiredValueException} = require('../exception/RequiredValueException');
const {IPAddressMalformedExcpetion} = require('../exception/IPAddressMalformedException');
const {TypeException} = require('../exception/TypeException');
/**
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */

/**
 * This function allows to validate if the required value 
 * @param {*} value to be verify
 * @param {String} errorMessage to be show if the value is not correct
 */
function validateRequiredFields(value, errorMessage){
    let isNumberAndNaN = isNaN(value) && typeof value == 'number';
    if(value == null || value.toString().length == 0 || isNumberAndNaN){
        throw new RequiredValueException(errorMessage);
    }
}

/**
 * This functions allows to validate the IP address
 * The IP must be between 0.0.0.0 and 255.255.255.255
 * @param {String} ipAddress to get validated
 * @param {String} errorMessage to be show if the value is not correct
 */
function validateIPAddress(ipAddress, errorMessage){
    let arrIPAddress = ipAddress.split('.');
    if(arrIPAddress.length != 4){
        throw new IPAddressMalformedExcpetion(errorMessage);
    }else{
        let initialNetID = parseInt(arrIPAddress[0]);
        let finalNetID = parseInt(arrIPAddress[1]);
        let initialHostID = parseInt(arrIPAddress[2]);
        let finalHostID = parseInt(arrIPAddress[3]);
        if((initialNetID<0||initialNetID>255)||(finalNetID<0||finalNetID>255)
            ||(initialHostID<0||initialHostID>255)||(finalHostID<0||finalHostID>255)){
            throw new IPAddressMalformedExcpetion(errorMessage);
        }
    }
}

/**
 * This function allows to validate if a value is numeric
 * @param {*} value to be verified
 * @param {String} errorMessage to be show if the value is not numeric
 */
function validateNumericType(value, errorMessage){
    if(typeof value != 'number'){
        throw new TypeException(errorMessage);
    }
}

/**
 * This function allows to validate the protocol
 * @param {Number} value to be validated
 * @param {String} errorMessage to be show if the value is -1
 */
function validateProtocol(value, errorMessage){
    
    if(value==-1){
        throw new RequiredValueException(errorMessage);
    }
}


module.exports = {
    validateRequiredFields,
    validateIPAddress,
    validateNumericType,
    validateProtocol
};