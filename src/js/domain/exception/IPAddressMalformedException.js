/**
 * This class is the IP Address malformed exceptions to the IP Address values
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class IPAddressMalformedExcpetion extends Error {

    /**
     * This allows to create an IPAddressMalformedExcpetion
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    IPAddressMalformedExcpetion
};