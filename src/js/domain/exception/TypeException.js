/**
 * This class is the type exception to the types validations
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class TypeException extends Error {
    /**
     * This allows to create a TypeException
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    TypeException
};