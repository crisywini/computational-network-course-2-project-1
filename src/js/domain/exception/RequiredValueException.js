/**
 * This class is the required value exception to the required values
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class RequiredValueException extends Error {
    /**
     * This allows to create a RequiredValueException
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    RequiredValueException
};