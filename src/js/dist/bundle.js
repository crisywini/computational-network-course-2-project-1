(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
const {
    DatagramIpService
} = require('../service/DatagramIPService')

const MTU = document.getElementById('mtu');
const DATAGRAM_LENGTH = document.getElementById('datagram-length');
const SOURCE_IP = document.getElementById('source-IP');
const DESTINATION_IP = document.getElementById('destination-IP');
const ICMP_PROTOCOL_RADIO_BUTTON = document.getElementById('ICMP');
const TCP_PROTOCOL_RADIO_BUTTON = document.getElementById('TCP');
const UDP_PROTOCOL_RADIO_BUTTON = document.getElementById('UDP');
const ANALYZE_BUTTON = document.getElementById('analyze_btn');
const BINARY_RESULT_LABEL = document.getElementById('binary-result-label');
const HEX_RESULT_LABEL = document.getElementById('hex-result-label');
const WIRESHARK_RESULT_LABEL = document.getElementById('wireshark-result-label');

const DATAGRAM_IP_SERVICE = new DatagramIpService();

function getProtocolSelected() {
    if (ICMP_PROTOCOL_RADIO_BUTTON.checked) {
        return 1;
    } else if (TCP_PROTOCOL_RADIO_BUTTON.checked) {
        return 6;
    } else if (UDP_PROTOCOL_RADIO_BUTTON.checked) {
        return 17;
    } else {
        return -1;
    }
}

function initData() {
    DATAGRAM_IP_SERVICE.mtu = parseInt(MTU.value);
    let protocol = getProtocolSelected();
    let datagramLength = parseInt(DATAGRAM_LENGTH.value.toString());
    DATAGRAM_IP_SERVICE.createDatagram(datagramLength, protocol, SOURCE_IP.value, DESTINATION_IP.value);
}

function cleanValues() {
    MTU.innerHTML = 0;
    DATAGRAM_LENGTH.innerHTML = 0;
    SOURCE_IP.innerHTML = '';
    DESTINATION_IP.innerHTML = '';
    ICMP_PROTOCOL_RADIO_BUTTON.checked = false;
    TCP_PROTOCOL_RADIO_BUTTON.checked = false;
    UDP_PROTOCOL_RADIO_BUTTON.checked = false;
}

ANALYZE_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    try {
        initData();
        cleanValues();
        HEX_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getHexRepresentation();
        WIRESHARK_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getWireSharkRepresentation().replaceAll('\n', '<br>');
        BINARY_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getBinaryRepresentation().replaceAll('\n', '<br>');
        initTableFragments();

    } catch (error) {
        console.trace(error);
        alert(error.message);
    }
});

function cleanTable(table) {
    table.innerHTML = "";
    let tableBody = document.createElement('tBody');
    let tr = document.createElement('tr');
    let th = document.createElement('th');
    th.innerText = "# Fragmento";
    tr.appendChild(th);
    tableBody.appendChild(tr);

    let th2 = document.createElement('th');
    th2.innerText = "IP Origen";
    tr.appendChild(th2);
    tableBody.appendChild(tr);

    let th3 = document.createElement('th');
    th3.innerText = "IP Destino";
    tr.appendChild(th3);
    tableBody.appendChild(tr);

    let th4 = document.createElement('th');
    th4.innerText = "ID";
    tr.appendChild(th4);
    tableBody.appendChild(tr);

    let th5 = document.createElement('th');
    th5.innerText = "FragmentOffset";
    tr.appendChild(th5);
    tableBody.appendChild(tr);

    let th6 = document.createElement('th');
    th6.innerText = "Longitud";
    tr.appendChild(th6);
    tableBody.appendChild(tr);
    table.appendChild(tableBody);
}


function cleanResults() {
    HEX_RESULT_LABEL.innerHTML = '';
    WIRESHARK_RESULT_LABEL.innerHTML = '';
    BINARY_RESULT_LABEL.innerHTML = '';
}

function initTableFragments() {
    let fragmentsTable = document.getElementById('fragments');
    cleanTable(fragmentsTable);

    let fragments = DATAGRAM_IP_SERVICE.getFragments();

    let tableBody = document.createElement('tBody');
    let i = 0;

    fragments.forEach(a => {
        let tr = document.createElement('tr');

        let td = document.createElement('td');
        td.innerText = i;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.sourceIP;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.destinationIP;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.id;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.fragmentOffset;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.totalLength;
        tr.appendChild(td);

        tr.addEventListener('click', (e) => {
            e.preventDefault();
            HEX_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getHexFormatPresentationFragment(a);
            WIRESHARK_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getWireSharkRepresentationFragment(a).replaceAll('\n', '<br>');
            BINARY_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getBinaryRepresentationFragment(a).replaceAll('\n', '<br>');
        });

        tableBody.appendChild(tr);
        i++;
    });
    fragmentsTable.appendChild(tableBody);
}
},{"../service/DatagramIPService":9}],2:[function(require,module,exports){
/**
 * This class is the IP Address malformed exceptions to the IP Address values
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class IPAddressMalformedExcpetion extends Error {

    /**
     * This allows to create an IPAddressMalformedExcpetion
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    IPAddressMalformedExcpetion
};
},{}],3:[function(require,module,exports){
/**
 * This class is the required value exception to the required values
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class RequiredValueException extends Error {
    /**
     * This allows to create a RequiredValueException
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    RequiredValueException
};
},{}],4:[function(require,module,exports){
/**
 * This class is the type exception to the types validations
 */
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class TypeException extends Error {
    /**
     * This allows to create a TypeException
     * @param {String} message error message to be show
     */
    constructor(message) {
        super(message);
    }
}

module.exports = {
    TypeException
};
},{}],5:[function(require,module,exports){

const {Header} = require('./Header');

/**
 * This class is the datagramIP
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class DatagramIP{
    /**
     * 
     * This constructor allows to create DatagramIP objects
     * 
     * @param {Header} header of the datagram needs the correct data
     * 
     */
    constructor(header){
        this.header = header;
    }
}

module.exports = {
    DatagramIP
};
},{"./Header":6}],6:[function(require,module,exports){
const {
    ConversionService
} = require('../../service/ConversionService');
const {
    validateRequiredFields,
    validateIPAddress,
    validateNumericType,
    validateProtocol
} = require('./Validator');

/**
 * This class is the header of the DatagramIP
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class Header {

    /**
     * This constructor allows to create Headers IP object
     * 
     * @param {Number} totalLength a number for the total length of the header
     * @param {Array} flags an array of numbers with only 3 positions [0, 1||0, 1||0] for the flags
     * @param {Number} fragmentOffset a number for the fragment off set
     * @param {Number} protocol could be TCP, ICMP or UDP ids 
     * @param {String} sourceIP an IP address in IPv4
     * @param {String} destinationIP an IP address in IPv4
     * 
     */
    constructor(totalLength, flags, fragmentOffset, protocol, sourceIP, destinationIP) {

        //Validate all the required fields

        validateRequiredFields(totalLength, "Es necesario ingresar la longitud total");
        validateRequiredFields(fragmentOffset, "Es necesario ingresar el desplazamiento");
        validateRequiredFields(protocol, "Es necesario ingresar el protocolo");
        validateRequiredFields(sourceIP, "Es necesario ingresar la dirección IP de origen");
        validateRequiredFields(destinationIP, "Es necesario ingresar la dirección IP de destino");
        validateProtocol(protocol, "Es necesario ingresar el protocolo");
        //Validate the numeric fields

        validateNumericType(totalLength, "La longitud total debe ser un número");
        validateNumericType(fragmentOffset, "El desplazamiento debe ser un número");
        //Validate the IP addresses

        validateIPAddress(sourceIP, "La dirección IP de origen no es correcta");
        validateIPAddress(destinationIP, "La dirección IP de destino no es correcta");


        this.version = 4;
        this.headerLength = 5;
        this.services = 0;
        this.totalLength = totalLength;
        this.flags = flags;
        this.fragmentOffset = fragmentOffset;
        this.protocol = protocol;
        this.sourceIP = sourceIP;
        this.destinationIP = destinationIP;
        this.id = this.generateID();
        this.ttl = this.generateTTL();
        this.checksum = this.getChecksum();
    }


    /**
     * This method generates the id for the header
     * @returns {Number} could be a number between 0-65535
     */
    generateID() {
        return Math.floor(Math.random() * (65535));
    }

    /**
     * This method generates the ttl for the header
     * @returns {Number} could be a number between 0-255
     */
    generateTTL() {
        return Math.floor(Math.random() * (255));
    }

    /**
     * 
     * This method allows to get the format presentation for the frontend
     * gets all the components of the header
     * 
     * @returns {String} with the hex format
     */
    getHexFormatPresentation() {
        let versionHex = ConversionService.getStringHexNumber(this.version);
        let headerLengthHex = ConversionService.getStringHexNumber(this.headerLength);
        let servicesHex = ConversionService.getStringHexNumber(this.services);
        let totalLengthHex = ConversionService.getStringHexNumber(this.totalLength);
        let idHex = ConversionService.getStringHexNumber(this.id);

        let flagsHex = this.flags.toString().replaceAll(",", ""); //3bits
        let offsetHex = ConversionService.getFormat13bits(ConversionService.getStringBinaryNumber(this.fragmentOffset)); //13 bits
        let flags_offset = parseInt(ConversionService.getStringDecimalNumber(flagsHex + offsetHex));
        let flags_offsetHex = ConversionService.getStringHexNumber(flags_offset);

        let ttlHex = ConversionService.getStringHexNumber(this.ttl);
        let protocolHex = ConversionService.getStringHexNumber(this.protocol);

        let sourceIPArray = this.sourceIP.split('.');


        let sourceIP1 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[0]));
        let sourceIP2 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[1]));
        let sourceIP3 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[2]));
        let sourceIP4 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[3]));

        let destinationIPArray = this.destinationIP.split('.');
        let destinationIP1 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[0]));
        let destinationIP2 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[1]));
        let destinationIP3 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[2]));
        let destinationIP4 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[3]));
        let totalLengthHexAux = ConversionService.getFormat16Bits(totalLengthHex);
        let flags_offsetAux = ConversionService.getFormat16Bits(flags_offsetHex);
        let out = versionHex + headerLengthHex + ' ' + ConversionService.getFormatHex(servicesHex) + ' ' +
            totalLengthHexAux.substring(0, (totalLengthHexAux.length / 2)) + ' ' + totalLengthHexAux.substring((totalLengthHexAux.length / 2), totalLengthHexAux.length) + ' ' +
            ConversionService.getFormatHex(idHex.substring(0, (idHex.length / 2))) + ' ' + ConversionService.getFormatHex(idHex.substring((idHex.length / 2), idHex.length)) + ' ' +
            flags_offsetAux.substring(0, (flags_offsetAux.length / 2)) + ' ' + flags_offsetAux.substring((flags_offsetAux.length / 2), flags_offsetAux.length) + ' ' +
            ConversionService.getFormatHex(ttlHex) + ' ' + ConversionService.getFormatHex(protocolHex) + ' ' +
            ConversionService.getFormatHex(this.checksum.substring(0, (this.checksum.length / 2))) + ' ' + ConversionService.getFormatHex(this.checksum.substring((this.checksum.length / 2), this.checksum.length)) + ' ' +
            ConversionService.getFormatHex(sourceIP1) + ' ' + ConversionService.getFormatHex(sourceIP2) + ' ' +
            ConversionService.getFormatHex(sourceIP3) + ' ' + ConversionService.getFormatHex(sourceIP4) + ' ' +
            ConversionService.getFormatHex(destinationIP1) + ' ' + ConversionService.getFormatHex(destinationIP2) + ' ' +
            ConversionService.getFormatHex(destinationIP3) + ' ' + ConversionService.getFormatHex(destinationIP4) + ' ';

        return out;
    }


    /**
     * This method allows to get the format in hex without the checksum
     * 
     * @returns {String} with the hex format
     */
    getHexFormatWithoutChecksum() {
        let versionHex = ConversionService.getStringHexNumber(this.version);
        let headerLengthHex = ConversionService.getStringHexNumber(this.headerLength);
        let servicesHex = ConversionService.getStringHexNumber(this.services);
        let totalLengthHex = ConversionService.getStringHexNumber(this.totalLength);
        let idHex = ConversionService.getStringHexNumber(this.id);

        let flagsHex = this.flags.toString().replaceAll(",", ""); //3bits
        let offsetHex = ConversionService.getFormat13bits(ConversionService.getStringBinaryNumber(this.fragmentOffset)); //13 bits
        let flags_offset = parseInt(ConversionService.getStringDecimalNumber(flagsHex + offsetHex));
        let flags_offsetHex = ConversionService.getStringHexNumber(flags_offset);

        let ttlHex = ConversionService.getStringHexNumber(this.ttl);
        let protocolHex = ConversionService.getStringHexNumber(this.protocol);

        let sourceIPArray = this.sourceIP.split('.');

        let sourceIP1 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[0]));
        let sourceIP2 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[1]));
        let sourceIP3 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[2]));
        let sourceIP4 = ConversionService.getStringHexNumber(parseInt(sourceIPArray[3]));

        let destinationIPArray = this.destinationIP.split('.');
        let destinationIP1 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[0]));
        let destinationIP2 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[1]));
        let destinationIP3 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[2]));
        let destinationIP4 = ConversionService.getStringHexNumber(parseInt(destinationIPArray[3]));
        let totalLengthHexAux = ConversionService.getFormat16Bits(totalLengthHex);
        let flags_offsetAux = ConversionService.getFormat16Bits(flags_offsetHex);
        let out = versionHex + headerLengthHex + ' ' + ConversionService.getFormatHex(servicesHex) + ' ' +
            totalLengthHexAux.substring(0, (totalLengthHexAux.length / 2)) + ' ' + totalLengthHexAux.substring((totalLengthHexAux.length / 2), totalLengthHexAux.length) + ' ' +
            ConversionService.getFormatHex(idHex.substring(0, (idHex.length / 2))) + ' ' + ConversionService.getFormatHex(idHex.substring((idHex.length / 2), idHex.length)) + ' ' +
            flags_offsetAux.substring(0, (flags_offsetAux.length / 2)) + ' ' + flags_offsetAux.substring((flags_offsetAux.length / 2), flags_offsetAux.length) + ' ' +
            ConversionService.getFormatHex(ttlHex) + ' ' + ConversionService.getFormatHex(protocolHex) + ' ' +
            ConversionService.getFormatHex(sourceIP1) + ' ' + ConversionService.getFormatHex(sourceIP2) + ' ' +
            ConversionService.getFormatHex(sourceIP3) + ' ' + ConversionService.getFormatHex(sourceIP4) + ' ' +
            ConversionService.getFormatHex(destinationIP1) + ' ' + ConversionService.getFormatHex(destinationIP2) + ' ' +
            ConversionService.getFormatHex(destinationIP3) + ' ' + ConversionService.getFormatHex(destinationIP4) + ' ';

        return out;
    }

    /**
     * This method allows to get the checksum
     * 
     * @returns {String} with the format
     */
    getChecksum() {
        let formatHex = this.getHexFormatWithoutChecksum().split(' ');
        let pairs = [];

        for (let i = 0; i < formatHex.length - 1; i += 2) {
            pairs.push(formatHex[i] + formatHex[i + 1]);
        }
        let checksum = 0;
        let numberA = pairs[0];
        let numberB = pairs[1];
        for (let i = 2; i < pairs.length; i++) {
            checksum = ConversionService.getStringHexNumber(ConversionService.sumHexNumbers(numberA, numberB));
            numberA = checksum;
            numberB = pairs[i];
        }
        checksum = ConversionService.getStringHexNumber(ConversionService.sumHexNumbers(numberA, numberB));
        return ConversionService.setComplementA1(checksum);
    }
    /**
     * This method allows to get the Wireshark presentation 
     * @returns {String} with the format required
     */
    getWireSharkRepresentation() {
        let out = "Internet Protocol Version: " + this.version +
            "\nLongitud de Encabezado: " + this.headerLength +
            "\nServicios Diferenciados:" + this.services +
            "\nLongitud Total:" + this.totalLength +
            "\nIdentificacion:" + this.id +
            "\nFlags: " + this.flags[0] +
            "\n DF:" + this.flags[1] +
            "\n MF:" + this.flags[2] +
            "\nDesplazamiento: " + this.fragmentOffset +
            "\nTiempo de Vida: " + this.ttl +
            "\nProtocolo: " + this.protocol +
            "\nChecksum: " + this.checksum +
            "\nDireccion  de  Origen: " + this.sourceIP +
            "\nDireccion  de  Destino: " + this.destinationIP;
        return out;
    }
    /**
     * This method allows to get the binary representation 
     * @returns {String} with the format 
     */
    getBinaryRepresentation() {
        let hex = this.getHexFormatPresentation();
        let hexArray = hex.split(' ');
        let binary = '';
        let counter = 0;
        for (let i = 0; i < hexArray.length - 1; i++) {
            let numberA = hexArray[i][0];
            let numberB = hexArray[i][1];
            let numberAConverted = ConversionService.getBinaryStringByHex(numberA);
            let numberBConverted = ConversionService.getBinaryStringByHex(numberB);
            let numberABinary = ConversionService.getFormat16Bits(numberAConverted);
            let numberBBinary = ConversionService.getFormat16Bits(numberBConverted);
            binary += ConversionService.addSpacesBinary(numberABinary) + ConversionService.addSpacesBinary(numberBBinary);
            counter += 8;
            if (counter == 32) {
                binary += "\n";
                counter = 0;
            }
        }
        return binary;
    }
}

module.exports = {
    Header
};
},{"../../service/ConversionService":8,"./Validator":7}],7:[function(require,module,exports){
const {RequiredValueException} = require('../exception/RequiredValueException');
const {IPAddressMalformedExcpetion} = require('../exception/IPAddressMalformedException');
const {TypeException} = require('../exception/TypeException');
/**
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */

/**
 * This function allows to validate if the required value 
 * @param {*} value to be verify
 * @param {String} errorMessage to be show if the value is not correct
 */
function validateRequiredFields(value, errorMessage){
    let isNumberAndNaN = isNaN(value) && typeof value == 'number';
    if(value == null || value.toString().length == 0 || isNumberAndNaN){
        throw new RequiredValueException(errorMessage);
    }
}

/**
 * This functions allows to validate the IP address
 * The IP must be between 0.0.0.0 and 255.255.255.255
 * @param {String} ipAddress to get validated
 * @param {String} errorMessage to be show if the value is not correct
 */
function validateIPAddress(ipAddress, errorMessage){
    let arrIPAddress = ipAddress.split('.');
    if(arrIPAddress.length != 4){
        throw new IPAddressMalformedExcpetion(errorMessage);
    }else{
        let initialNetID = parseInt(arrIPAddress[0]);
        let finalNetID = parseInt(arrIPAddress[1]);
        let initialHostID = parseInt(arrIPAddress[2]);
        let finalHostID = parseInt(arrIPAddress[3]);
        if((initialNetID<0||initialNetID>255)||(finalNetID<0||finalNetID>255)
            ||(initialHostID<0||initialHostID>255)||(finalHostID<0||finalHostID>255)){
            throw new IPAddressMalformedExcpetion(errorMessage);
        }
    }
}

/**
 * This function allows to validate if a value is numeric
 * @param {*} value to be verified
 * @param {String} errorMessage to be show if the value is not numeric
 */
function validateNumericType(value, errorMessage){
    if(typeof value != 'number'){
        throw new TypeException(errorMessage);
    }
}

/**
 * This function allows to validate the protocol
 * @param {Number} value to be validated
 * @param {String} errorMessage to be show if the value is -1
 */
function validateProtocol(value, errorMessage){
    
    if(value==-1){
        throw new RequiredValueException(errorMessage);
    }
}


module.exports = {
    validateRequiredFields,
    validateIPAddress,
    validateNumericType,
    validateProtocol
};
},{"../exception/IPAddressMalformedException":2,"../exception/RequiredValueException":3,"../exception/TypeException":4}],8:[function(require,module,exports){
class ConversionService {

    /**
     * This method allows to convert a decimal number to binary String
     * 
     * @param {Number} decimalNumber 
     * @returns {String} with the number in the binary form
     */
    static getStringBinaryNumber(decimalNumber) {
        return decimalNumber.toString(2);
    }
    /**
     * This method allows to convert a decimal number to hex String 
     * @param {Number} decimalNumber 
     * @returns {String} with the number in hex form
     */
    static getStringHexNumber(decimalNumber) {
        return decimalNumber.toString(16);
    }

    /**
     * This method allows to get the decimal representation of a binary String
     * 
     * @param {String} binaryNumberString 
     */
    static getStringDecimalNumber(binaryNumberString) {
        return parseInt(binaryNumberString, 2) + "";
    }

    /**
     * This method allows to get the decimal representation of a binary String
     * 
     * @param {String} hexNumber 
     */
    static getStringDecimalNumberFromHex(hexNumber) {
        return parseInt(hexNumber, 16) + "";
    }

    /**
     * This method allows to get the sum between two numbers in decimal
     * 
     * @param {String} hexNumber1 
     * @param {String} hexNumber2 
     * @returns {Number} 
     */
    static sumHexNumbers(hexNumber1, hexNumber2) {
        let decimalNumber1 = parseInt(this.getStringDecimalNumberFromHex(hexNumber1));
        let decimalNumber2 = parseInt(this.getStringDecimalNumberFromHex(hexNumber2));
        let result = decimalNumber1 + decimalNumber2;

        let strResult = result.toString(16);
        while (strResult.length > 4) {
            let rightNumber = parseInt(strResult.substring(0, 1), 16);
            let leftNumber = parseInt(strResult.substring(1, strResult.length), 16);
            result = rightNumber + leftNumber;
            strResult = result.toString(16);
        }
        return result;
    }

    /**
     * This method allows to get de hex number of a binary numbrer
     * @param {Number} binaryNumber 
     * @returns {String}
     */
    static getHexNumberByBinary(binaryNumber) {
        let decimalTemporal = parseInt(binaryNumber, 2);
        let hexNumber = decimalTemporal.toString(16);
        return hexNumber;
    }

    /**
     * This method allows to get de hex number of a binary String
     * @param {String} binaryNumber 
     * @returns {String}
     */
    static getHexNumberByBinaryString(binaryString) {
        let binaryNumber = parseInt(binaryString);
        let decimalTemporal = parseInt(binaryNumber, 2);
        let hexNumber = decimalTemporal.toString(16);
        return hexNumber;
    }

    /**
     * This method allows to apply the Complement A1 to a number
     * @param {String} hexNumber to apply de Complement
     * @returns {String} with the complement applied
     */

    static setComplementA1(hexNumber) {
        let decimalNumber = parseInt(hexNumber, 16);
        let str = 'FFFF';
        let numberStr = parseInt(str, 16);
        return this.getStringHexNumber(numberStr - decimalNumber);
    }

    /**
     * This methof allows to get the binary number for a hex number
     * @param {String} hex 
     * @returns number
     */
    static getBinaryNumberByHex(hex) {
        let hexNumber = parseInt(hex, 16) + "";
        let binaryNumber = parseInt(hexNumber, 2);
        return binaryNumber;
    }

    /**
     * This method allows to get the binary String by a hex number
     * @param {String} hex 
     * @returns String
     */
    static getBinaryStringByHex(hex) {
        let hexNumber = parseInt(hex, 16);
        let binaryNumber = hexNumber.toString(2);
        return binaryNumber + "";
    }

    /**
     * This method allows to get hex format 
     * 
     * @param {String} string 
     */
    static getFormatHex(string) {
        if (string.length < 2) {
            string = '0' + string;
        }
        return string;
    }

    /**
     * This method allows to get the format 16 bits 
     * @param {String} string 
     */
    static getFormat16Bits(string) {
        let substract = 4 - string.length;
        for (let i = 0; i < substract; i++) {
            string = '0' + string;
        }
        return string;
    }

    /**
     * This method allows to get the 13 format bits
     * @param {String} string 
     */
    static getFormat13bits(string) {
        let substract = 13 - string.length;
        for (let i = 0; i < substract; i++) {
            string = '0' + string;
        }
        return string;
    }
    /**
     * This method allows the add spaces to a binary
     * 
     * @param {String} binaryString 
     */
    static addSpacesBinary(binaryString) {
        let result = '';
        for (let i = 0; i < binaryString.length; i++) {
            result += binaryString[i] + " ";
        }
        return result;
    }
}

module.exports = {
    ConversionService
}
},{}],9:[function(require,module,exports){
/**
 * This script allows to do all the needed services to process the information
 */
const {
    DatagramIP
} = require('../domain/model/DatagramIP');
const {
    Header
} = require('../domain/model/Header');
const {
    validateNumericType,
    validateRequiredFields
} = require('../domain/model/Validator');
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class DatagramIpService {

    /**
     * Constructor method for the class DatagramIpService 
     * allows to get all the services of the Datagram
     * 
     * @param {DatagramIP} datagramIP 
     * @param {number} mtu
     */
    constructor() {
        this.datagramIP;
        this.MTU;
    }
    /**
     * Set method to mtu
     * @param {*} mtu 
     */
    set mtu(mtu) {
        validateRequiredFields(mtu, "El valor del MTU debe ser ingresado");

        validateNumericType(mtu, "El valor del MTU debe ser un número");
        this.MTU = mtu;
    }


    /**
     * This method allows to get the fragments 
     * @param {DatagramIP} fragment
     * @return {Array} of fragments
     */
    getFragments() {

        let header = this.datagramIP.header;
        let totalLengthDatagram = header.totalLength;
        let fragments = [];
        let counter = 0;
        let lengthFragment = 0; //Allows to take a copy and process it to each fragment
        let mtu_ = this.MTU - 20;
        while (counter < totalLengthDatagram) {
            if (counter + mtu_ >= totalLengthDatagram) {
                lengthFragment = totalLengthDatagram - counter;
            } else {
                lengthFragment = mtu_;
            }
            let flags = [0, 0, 1];
            let fragmentOffset = counter;
            //(totalLength, flags, fragmentOffset, protocol, sourceIP, destinationIP)
            let newHeader = new Header(lengthFragment + 20, flags, fragmentOffset, header.protocol,
                header.sourceIP, header.destinationIP);
            newHeader.id = header.id;
            newHeader.ttl = header.ttl;
            newHeader.checksum = newHeader.getChecksum();

            fragments.push(new DatagramIP(newHeader));

            counter += lengthFragment;
        }
        let flagsLastOne = [0, 1, 0];
        fragments[fragments.length - 1].header.flags = flagsLastOne;
        return fragments;
    }
    /**
     * This method allows create a datagram
     * @param {Number} totalLength  of the datagram
     * @param {Number} protocol of the datagram 
     * @param {String} sourceIP of the datagram
     * @param {String} destinationIP of the datagram
     */
    createDatagram(totalLength, protocol, sourceIP, destinationIP) {
        let flags = [0, 0, 0];
        if (this.MTU < totalLength) {
            flags = [0, 0, 1];
        }
        let header = new Header(totalLength, flags, 0, protocol, sourceIP, destinationIP);
        let datagramIP = new DatagramIP(header);

        this.datagramIP = datagramIP
    }
    /**
     * This method allows to get the hex representation for the header
     */
    getHexRepresentation() {
        return this.datagramIP.header.getHexFormatPresentation();
    }
    /**
     * This method allows to get the wireshark representation of the header
     */
    getWireSharkRepresentation() {
        return this.datagramIP.header.getWireSharkRepresentation();
    }
    /**
     * This method allows to get the binary representation of the header
     */
    getBinaryRepresentation() {
        return this.datagramIP.header.getBinaryRepresentation();
    }
    /**
     * This method allows to get the hex format representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getHexFormatPresentationFragment(fragment) {
        return fragment.header.getHexFormatPresentation();
    }
    /**
     * This method allows to get the binary representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getBinaryRepresentationFragment(fragment) {
        return fragment.header.getBinaryRepresentation();
    }
    /**
     * This method allows to get the wireshark representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getWireSharkRepresentationFragment(fragment) {
        return fragment.header.getWireSharkRepresentation();
    }
}

module.exports = {
    DatagramIpService
}
},{"../domain/model/DatagramIP":5,"../domain/model/Header":6,"../domain/model/Validator":7}]},{},[1]);
