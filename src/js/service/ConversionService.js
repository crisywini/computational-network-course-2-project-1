class ConversionService {

    /**
     * This method allows to convert a decimal number to binary String
     * 
     * @param {Number} decimalNumber 
     * @returns {String} with the number in the binary form
     */
    static getStringBinaryNumber(decimalNumber) {
        return decimalNumber.toString(2);
    }
    /**
     * This method allows to convert a decimal number to hex String 
     * @param {Number} decimalNumber 
     * @returns {String} with the number in hex form
     */
    static getStringHexNumber(decimalNumber) {
        return decimalNumber.toString(16);
    }

    /**
     * This method allows to get the decimal representation of a binary String
     * 
     * @param {String} binaryNumberString 
     */
    static getStringDecimalNumber(binaryNumberString) {
        return parseInt(binaryNumberString, 2) + "";
    }

    /**
     * This method allows to get the decimal representation of a binary String
     * 
     * @param {String} hexNumber 
     */
    static getStringDecimalNumberFromHex(hexNumber) {
        return parseInt(hexNumber, 16) + "";
    }

    /**
     * This method allows to get the sum between two numbers in decimal
     * 
     * @param {String} hexNumber1 
     * @param {String} hexNumber2 
     * @returns {Number} 
     */
    static sumHexNumbers(hexNumber1, hexNumber2) {
        let decimalNumber1 = parseInt(this.getStringDecimalNumberFromHex(hexNumber1));
        let decimalNumber2 = parseInt(this.getStringDecimalNumberFromHex(hexNumber2));
        let result = decimalNumber1 + decimalNumber2;

        let strResult = result.toString(16);
        while (strResult.length > 4) {
            let rightNumber = parseInt(strResult.substring(0, 1), 16);
            let leftNumber = parseInt(strResult.substring(1, strResult.length), 16);
            result = rightNumber + leftNumber;
            strResult = result.toString(16);
        }
        return result;
    }

    /**
     * This method allows to get de hex number of a binary numbrer
     * @param {Number} binaryNumber 
     * @returns {String}
     */
    static getHexNumberByBinary(binaryNumber) {
        let decimalTemporal = parseInt(binaryNumber, 2);
        let hexNumber = decimalTemporal.toString(16);
        return hexNumber;
    }

    /**
     * This method allows to get de hex number of a binary String
     * @param {String} binaryNumber 
     * @returns {String}
     */
    static getHexNumberByBinaryString(binaryString) {
        let binaryNumber = parseInt(binaryString);
        let decimalTemporal = parseInt(binaryNumber, 2);
        let hexNumber = decimalTemporal.toString(16);
        return hexNumber;
    }

    /**
     * This method allows to apply the Complement A1 to a number
     * @param {String} hexNumber to apply de Complement
     * @returns {String} with the complement applied
     */

    static setComplementA1(hexNumber) {
        let decimalNumber = parseInt(hexNumber, 16);
        let str = 'FFFF';
        let numberStr = parseInt(str, 16);
        return this.getStringHexNumber(numberStr - decimalNumber);
    }

    /**
     * This methof allows to get the binary number for a hex number
     * @param {String} hex 
     * @returns number
     */
    static getBinaryNumberByHex(hex) {
        let hexNumber = parseInt(hex, 16) + "";
        let binaryNumber = parseInt(hexNumber, 2);
        return binaryNumber;
    }

    /**
     * This method allows to get the binary String by a hex number
     * @param {String} hex 
     * @returns String
     */
    static getBinaryStringByHex(hex) {
        let hexNumber = parseInt(hex, 16);
        let binaryNumber = hexNumber.toString(2);
        return binaryNumber + "";
    }

    /**
     * This method allows to get hex format 
     * 
     * @param {String} string 
     */
    static getFormatHex(string) {
        if (string.length < 2) {
            string = '0' + string;
        }
        return string;
    }

    /**
     * This method allows to get the format 16 bits 
     * @param {String} string 
     */
    static getFormat16Bits(string) {
        let substract = 4 - string.length;
        for (let i = 0; i < substract; i++) {
            string = '0' + string;
        }
        return string;
    }

    /**
     * This method allows to get the 13 format bits
     * @param {String} string 
     */
    static getFormat13bits(string) {
        let substract = 13 - string.length;
        for (let i = 0; i < substract; i++) {
            string = '0' + string;
        }
        return string;
    }
    /**
     * This method allows the add spaces to a binary
     * 
     * @param {String} binaryString 
     */
    static addSpacesBinary(binaryString) {
        let result = '';
        for (let i = 0; i < binaryString.length; i++) {
            result += binaryString[i] + " ";
        }
        return result;
    }
}

module.exports = {
    ConversionService
}