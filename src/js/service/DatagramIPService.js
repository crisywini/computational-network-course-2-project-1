/**
 * This script allows to do all the needed services to process the information
 */
const {
    DatagramIP
} = require('../domain/model/DatagramIP');
const {
    Header
} = require('../domain/model/Header');
const {
    validateNumericType,
    validateRequiredFields
} = require('../domain/model/Validator');
/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
class DatagramIpService {

    /**
     * Constructor method for the class DatagramIpService 
     * allows to get all the services of the Datagram
     * 
     * @param {DatagramIP} datagramIP 
     * @param {number} mtu
     */
    constructor() {
        this.datagramIP;
        this.MTU;
    }
    /**
     * Set method to mtu
     * @param {*} mtu 
     */
    set mtu(mtu) {
        validateRequiredFields(mtu, "El valor del MTU debe ser ingresado");

        validateNumericType(mtu, "El valor del MTU debe ser un número");
        this.MTU = mtu;
    }


    /**
     * This method allows to get the fragments 
     * @param {DatagramIP} fragment
     * @return {Array} of fragments
     */
    getFragments() {

        let header = this.datagramIP.header;
        let totalLengthDatagram = header.totalLength;
        let fragments = [];
        let counter = 0;
        let lengthFragment = 0; //Allows to take a copy and process it to each fragment
        let mtu_ = this.MTU - 20;
        while (counter < totalLengthDatagram) {
            if (counter + mtu_ >= totalLengthDatagram) {
                lengthFragment = totalLengthDatagram - counter;
            } else {
                lengthFragment = mtu_;
            }
            let flags = [0, 0, 1];
            let fragmentOffset = counter;
            //(totalLength, flags, fragmentOffset, protocol, sourceIP, destinationIP)
            let newHeader = new Header(lengthFragment + 20, flags, fragmentOffset, header.protocol,
                header.sourceIP, header.destinationIP);
            newHeader.id = header.id;
            newHeader.ttl = header.ttl;
            newHeader.checksum = newHeader.getChecksum();

            fragments.push(new DatagramIP(newHeader));

            counter += lengthFragment;
        }
        let flagsLastOne = [0, 1, 0];
        fragments[fragments.length - 1].header.flags = flagsLastOne;
        return fragments;
    }
    /**
     * This method allows create a datagram
     * @param {Number} totalLength  of the datagram
     * @param {Number} protocol of the datagram 
     * @param {String} sourceIP of the datagram
     * @param {String} destinationIP of the datagram
     */
    createDatagram(totalLength, protocol, sourceIP, destinationIP) {
        let flags = [0, 0, 0];
        if (this.MTU < totalLength) {
            flags = [0, 0, 1];
        }
        let header = new Header(totalLength, flags, 0, protocol, sourceIP, destinationIP);
        let datagramIP = new DatagramIP(header);

        this.datagramIP = datagramIP
    }
    /**
     * This method allows to get the hex representation for the header
     */
    getHexRepresentation() {
        return this.datagramIP.header.getHexFormatPresentation();
    }
    /**
     * This method allows to get the wireshark representation of the header
     */
    getWireSharkRepresentation() {
        return this.datagramIP.header.getWireSharkRepresentation();
    }
    /**
     * This method allows to get the binary representation of the header
     */
    getBinaryRepresentation() {
        return this.datagramIP.header.getBinaryRepresentation();
    }
    /**
     * This method allows to get the hex format representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getHexFormatPresentationFragment(fragment) {
        return fragment.header.getHexFormatPresentation();
    }
    /**
     * This method allows to get the binary representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getBinaryRepresentationFragment(fragment) {
        return fragment.header.getBinaryRepresentation();
    }
    /**
     * This method allows to get the wireshark representation of one fragment
     * @param {DatagramIP} fragment 
     */
    getWireSharkRepresentationFragment(fragment) {
        return fragment.header.getWireSharkRepresentation();
    }
}

module.exports = {
    DatagramIpService
}