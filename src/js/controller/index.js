/**
 * 
 * @author Luisa Fernanda Cotte Sánchez
 * @author Daniel Alejandro Loaiza
 * @author Cristian Giovanny Sánchez Pineda
 */
const {
    DatagramIpService
} = require('../service/DatagramIPService')

const MTU = document.getElementById('mtu');
const DATAGRAM_LENGTH = document.getElementById('datagram-length');
const SOURCE_IP = document.getElementById('source-IP');
const DESTINATION_IP = document.getElementById('destination-IP');
const ICMP_PROTOCOL_RADIO_BUTTON = document.getElementById('ICMP');
const TCP_PROTOCOL_RADIO_BUTTON = document.getElementById('TCP');
const UDP_PROTOCOL_RADIO_BUTTON = document.getElementById('UDP');
const ANALYZE_BUTTON = document.getElementById('analyze_btn');
const BINARY_RESULT_LABEL = document.getElementById('binary-result-label');
const HEX_RESULT_LABEL = document.getElementById('hex-result-label');
const WIRESHARK_RESULT_LABEL = document.getElementById('wireshark-result-label');

const DATAGRAM_IP_SERVICE = new DatagramIpService();

function getProtocolSelected() {
    if (ICMP_PROTOCOL_RADIO_BUTTON.checked) {
        return 1;
    } else if (TCP_PROTOCOL_RADIO_BUTTON.checked) {
        return 6;
    } else if (UDP_PROTOCOL_RADIO_BUTTON.checked) {
        return 17;
    } else {
        return -1;
    }
}

function initData() {
    DATAGRAM_IP_SERVICE.mtu = parseInt(MTU.value);
    let protocol = getProtocolSelected();
    let datagramLength = parseInt(DATAGRAM_LENGTH.value.toString());
    DATAGRAM_IP_SERVICE.createDatagram(datagramLength, protocol, SOURCE_IP.value, DESTINATION_IP.value);
}

function cleanValues() {
    MTU.innerHTML = 0;
    DATAGRAM_LENGTH.innerHTML = 0;
    SOURCE_IP.innerHTML = '';
    DESTINATION_IP.innerHTML = '';
    ICMP_PROTOCOL_RADIO_BUTTON.checked = false;
    TCP_PROTOCOL_RADIO_BUTTON.checked = false;
    UDP_PROTOCOL_RADIO_BUTTON.checked = false;
}

ANALYZE_BUTTON.addEventListener('click', (e) => {
    e.preventDefault();
    try {
        initData();
        cleanValues();
        HEX_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getHexRepresentation();
        WIRESHARK_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getWireSharkRepresentation().replaceAll('\n', '<br>');
        BINARY_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getBinaryRepresentation().replaceAll('\n', '<br>');
        initTableFragments();

    } catch (error) {
        console.trace(error);
        alert(error.message);
    }
});

function cleanTable(table) {
    table.innerHTML = "";
    let tableBody = document.createElement('tBody');
    let tr = document.createElement('tr');
    let th = document.createElement('th');
    th.innerText = "# Fragmento";
    tr.appendChild(th);
    tableBody.appendChild(tr);

    let th2 = document.createElement('th');
    th2.innerText = "IP Origen";
    tr.appendChild(th2);
    tableBody.appendChild(tr);

    let th3 = document.createElement('th');
    th3.innerText = "IP Destino";
    tr.appendChild(th3);
    tableBody.appendChild(tr);

    let th4 = document.createElement('th');
    th4.innerText = "ID";
    tr.appendChild(th4);
    tableBody.appendChild(tr);

    let th5 = document.createElement('th');
    th5.innerText = "FragmentOffset";
    tr.appendChild(th5);
    tableBody.appendChild(tr);

    let th6 = document.createElement('th');
    th6.innerText = "Longitud";
    tr.appendChild(th6);
    tableBody.appendChild(tr);
    table.appendChild(tableBody);
}


function cleanResults() {
    HEX_RESULT_LABEL.innerHTML = '';
    WIRESHARK_RESULT_LABEL.innerHTML = '';
    BINARY_RESULT_LABEL.innerHTML = '';
}

function initTableFragments() {
    let fragmentsTable = document.getElementById('fragments');
    cleanTable(fragmentsTable);

    let fragments = DATAGRAM_IP_SERVICE.getFragments();

    let tableBody = document.createElement('tBody');
    let i = 0;

    fragments.forEach(a => {
        let tr = document.createElement('tr');

        let td = document.createElement('td');
        td.innerText = i;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.sourceIP;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.destinationIP;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.id;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.fragmentOffset;
        tr.appendChild(td);

        td = document.createElement('td');
        td.innerText = a.header.totalLength;
        tr.appendChild(td);

        tr.addEventListener('click', (e) => {
            e.preventDefault();
            HEX_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getHexFormatPresentationFragment(a);
            WIRESHARK_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getWireSharkRepresentationFragment(a).replaceAll('\n', '<br>');
            BINARY_RESULT_LABEL.innerHTML = DATAGRAM_IP_SERVICE.getBinaryRepresentationFragment(a).replaceAll('\n', '<br>');
        });

        tableBody.appendChild(tr);
        i++;
    });
    fragmentsTable.appendChild(tableBody);
}